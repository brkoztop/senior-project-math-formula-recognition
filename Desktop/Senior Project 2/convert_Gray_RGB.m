clear all
imds = imageDatastore("/Users/burakoztop/Desktop/Senior Project/handwrittenmathsymbols/extracted_images", ...
    'IncludeSubfolders',true,'LabelSource','foldernames');
for i=1:numel(imds.Files)
    I=imread(imds.Files{i});
    II=cat(3,I,I,I);
    s=imds.Files{i};
    imwrite(II,s);
end