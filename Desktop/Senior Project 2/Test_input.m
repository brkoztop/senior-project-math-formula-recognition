I = imread("Test.png");
inputSize = net.Layers(1).InputSize;
I = imresize(I,inputSize(1:2));
label = classify(net,I);
figure
imshow(I)
title(string(label))