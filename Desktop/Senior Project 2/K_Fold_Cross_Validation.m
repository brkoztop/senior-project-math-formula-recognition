clear all
close all
imds = imageDatastore('/Users/burakoztop/Desktop/Senior Project/handwrittenmathsymbols/extracted_images', ...
    'IncludeSubfolders',true,'LabelSource','foldernames');


[imd1 imd2 imd3 imd4 imd5] = splitEachLabel(imds,0.20,0.20,0.20,0.20,0.20);
partStores{1} = imd1.Files ;
partStores{2} = imd2.Files ;
partStores{3} = imd3.Files ;
partStores{4} = imd4.Files ;
partStores{5} = imd5.Files ;
k = 5;
idx = crossvalind('Kfold', k, k);   %crossvalind generates cross-validation indices
for foldi = 1 :k
    test_idx = (idx == foldi);
    train_idx = ~test_idx;
    imdsValidation = imageDatastore(partStores{test_idx}, 'IncludeSubfolders', true,'LabelSource', 'foldernames');
    imdsTrain = imageDatastore(cat(1, partStores{train_idx}), 'IncludeSubfolders', true,'LabelSource', 'foldernames');    
    [no_of_TrainImages ~]=size(imdsTrain.Files);
    numClasses = numel(categories(imdsValidation.Labels));
    
layers = [
    imageInputLayer([45 45 1])
    
    convolution2dLayer(3,8,'Padding','same')
    batchNormalizationLayer
    reluLayer
    maxPooling2dLayer(2,'Stride',2)
    
    convolution2dLayer(3,16,'Padding','same')
    batchNormalizationLayer
    reluLayer
    maxPooling2dLayer(2,'Stride',2)
    
    convolution2dLayer(3,32,'Padding','same')
    batchNormalizationLayer
    reluLayer
    maxPooling2dLayer(2,'Stride',2)
    
    convolution2dLayer(3,128,'Padding','same')
    batchNormalizationLayer
    reluLayer
    maxPooling2dLayer(2,'Stride',2)
    
    convolution2dLayer(3,128,'Padding','same')
    batchNormalizationLayer
    reluLayer
    
    fullyConnectedLayer(numClasses)
    softmaxLayer
    classificationLayer];
options = trainingOptions('sgdm', ...
    'InitialLearnRate',0.01, ...
    'MaxEpochs',4, ...
    'Shuffle','every-epoch', ...
    'ValidationData',imdsValidation, ...
    'ValidationFrequency',30, ...
    'Verbose',false, ...
    'Plots','training-progress');
net = trainNetwork(imdsTrain,layers,options);

YPred = classify(net,imdsValidation);
YValidation = imdsValidation.Labels;

Accuracy(foldi) = sum(YPred == YValidation)/numel(YValidation);
fprintf("Fold #%d: Accuracy=%.2f \n",foldi,Accuracy(foldi)*100)     
end
avg=sum(Accuracy)/5;
fprintf("The System Accuracy is %.2f \n",avg*100)
